//
//  ApiService.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 3/7/21.
//

import Foundation

class ApiService {

    func requestNews(selectedTopic: RequestTopic, completion: @escaping (_ news: NewsModel?, _ error: Errors?) -> Void) {
        
        let urlBody = getUrlBody(by: selectedTopic)
        guard let url = URL(string: URLS.baseTopRUHedlinesEndpoint + urlBody + "&apiKey=\(Constants.APIKey)") else {
            completion(nil, .urlFail)
            return
        }
        // короткий вариант, когда у нас только query параметры
        var request = URLRequest(url: url)
        request.httpMethod = HttpMethod.get.rawValue
//        request.configure(.post)

        // вариант, когда мы энкодим модель
       /* do {
            request.httpBody = try JSONEncoder().encode(filter)
        } catch {
            print(error)
            return
        } */

        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if let error = self.handleErrors(response, error) {
                completion(nil, error)
                return
            }

            guard let data = data else {
                completion(nil, .serverNotFound)
                return
            }

            // будет работать, но тяжело ловить ошибки
//            let result = try? JSONDecoder().decode(NewsModel.self, from: data)

            // правильный способ
            do {
                let news = try JSONDecoder().decode(NewsModel.self, from: data)
                completion(news, nil)
            } catch {
                print(error)
                completion(nil, nil)
            }
        }

        task.resume()
    }




    func handleErrors(_ response: URLResponse?, _ error: Error?) -> Errors? {
        guard let response = response as? HTTPURLResponse,
              error == nil else {
            return .serverNotFound
        }

        if response.statusCode == 200 {
            return nil
        }

        if response.statusCode == 404 {
            return .serverNotFound
        }

        if response.statusCode == 400 {
            return .badResponse
        }

        return .undefined
    }
    
    private func getUrlBody(by selectedTopic: RequestTopic) -> String {
        var topic = ""
        switch selectedTopic {
        case .business:
            topic = RequestTopic.business.rawValue
        case .science:
            topic = RequestTopic.science.rawValue
        case .sport:
            topic = RequestTopic.sport.rawValue
        case .politics:
            topic = RequestTopic.politics.rawValue
        case .technology:
            topic = RequestTopic.technology.rawValue
        }
        return topic
    }
}


extension URLRequest {
    mutating func configure(
        _ method: HttpMethod,
        _ parameters: [String: Any?]? = nil
    ) {
//        self.addValue("Bearer \(AuthenticationService.shared.accessToken ?? "")",
//            forHTTPHeaderField: "Authorization")
//        self.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        self.httpMethod = method.rawValue
        if let strongParameters = parameters, !strongParameters.isEmpty {
            self.httpBody = try? JSONSerialization.data(withJSONObject: strongParameters)
        }
    }
}

enum HttpMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
}

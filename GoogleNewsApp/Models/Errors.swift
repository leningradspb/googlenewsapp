//
//  Errors.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 3/7/21.
//

import Foundation

enum Errors {
    case undefined
    case serverNotFound
    case badResponse
    case internalServerError
    case urlFail

    var message: String {
        var message = ""
        switch self {
        case .undefined:
            message = "Неизвестная ошибка"
        case .badResponse:
            message = "Сервер вернул ошибку.\nПовторите попытку позже."
        case .internalServerError:
            message = "Сервер вернул ошибку. Внутренняя ошибка сервера.\nПовторите попытку позже."
        case .serverNotFound:
            message = "Не удается подключиться к серверу. \nПроверьте соединение."
        case .urlFail:
        message = "Нет URL."
        }

        return message
    }
}

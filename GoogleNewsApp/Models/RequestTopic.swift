//
//  RequestTopic.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 25.03.2021.
//

import Foundation

enum RequestTopic: String {
    case sport = "sport"
    case business = "business"
    case politics = "politics"
    case technology = "technology"
    case science = "science"
}

//
//  Colors.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 25.03.2021.
//

import UIKit

struct Colors {
    static let mainBlue = UIColor(hex: "#17408B")
}

//
//  NewsModel.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 3/7/21.
//

import Foundation

struct NewsModel: Codable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]?
}

struct Article: Codable {
    let source: Source?
    let author, title, description, url, urlToImage, publishedAt, content: String?
}

struct Source: Codable {
    let id, name: String?
}

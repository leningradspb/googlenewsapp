//
//  Constants.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 3/7/21.
//

import Foundation

struct Constants {
    static let baseURL = "https://newsapi.org/v2/"
    static let APIKey = "b9939bffc2574cd38f692e40872837a5"
}

struct URLS {
    static let sources = Constants.baseURL + "sources/"
    static let everything = Constants.baseURL + "everything/"
    static private let topHeadlines = Constants.baseURL + "top-headlines"
    static let baseTopRUHedlinesEndpoint = topHeadlines + "?country=ru&category="
    static let topSportNews = topHeadlines + "?country=ru&category=sport"
    //https://newsapi.org/v2/everything?q=Apple&from=2021-03-07&sortBy=popularity&apiKey=b9939bffc2574cd38f692e40872837a5
}

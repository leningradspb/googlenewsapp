//
//  ViewController.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 3/7/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    private let topics = ["⚽️ Спорт", "🖥 Бизнес", "🤝 Политика", "📱 Техника", "🤓 Наука"]
    private let requestTopics = [RequestTopic.sport, RequestTopic.business, RequestTopic.politics, RequestTopic.technology, RequestTopic.science]
    private var selectedRequestTopic = RequestTopic.sport
    private var selectedType = "⚽️ Спорт"
    private let toDetailNews = "toDetailNews"
    private var news: NewsModel?
    private var article: Article?
    
    private let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupTableView()
        setupCollectionView()
       requestNewsBy(selectedRequestTopic)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == toDetailNews {
            guard let vc = segue.destination as? DetailNewsVC, let selectedArticle = article else { return }
            vc.article = selectedArticle
        }
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

    private func showDetailNews() {
        performSegue(withIdentifier: toDetailNews, sender: self)
    }
    
    private func requestNewsBy(_ selectedTopic: RequestTopic) {
        ApiService().requestNews(selectedTopic: selectedTopic) { [weak self] news, error in
            guard let self = self else { return }
            print(news)
            DispatchQueue.main.async {
                self.news = news
                self.tableView.reloadData()
            }
        }
    }
    
    @objc private func refresh(sender: UIRefreshControl) {
        requestNewsBy(selectedRequestTopic)
        sender.endRefreshing()
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        topics.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionCell", for: indexPath) as! NewsCollectionCell
        let text = topics[indexPath.row]
        cell.update(with: text, selectedType: selectedType)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let contentOffset = collectionView.contentOffset
        selectedType = topics[indexPath.row]
        selectedRequestTopic = requestTopics[indexPath.row]
        requestNewsBy(selectedRequestTopic)
        self.collectionView.reloadData()
        self.collectionView.layoutIfNeeded()
        self.collectionView.setContentOffset(contentOffset, animated: false)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        news?.articles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableCell", for: indexPath) as! NewsTableCell
        
        if let model = news?.articles?[indexPath.row] {
            cell.update(with: model)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        article = news?.articles?[indexPath.row]
        showDetailNews()
    }
}

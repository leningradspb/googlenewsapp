//
//  DetailNewsVC.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 25.03.2021.
//

import UIKit
import SafariServices

class DetailNewsVC: UIViewController {
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var readButton: UIButton!
    
    @IBAction func readTapped(_ sender: Any) {
        showFullNews()
    }
    
    var article: Article!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    private func setupUI() {
        navigationController?.navigationBar.tintColor = .white
        
        if let urlToImage = article.urlToImage, let url = URL(string: urlToImage) {
            headerImageView.kf.setImage(with: url)
        }
        
        titleLabel.text = article.title
        descriptionLabel.text = article.description
        readButton.layer.cornerRadius = 10
    }
    
    func showFullNews() {
        if let urlString = article.url, let url = URL(string: urlString) {
            let config = SFSafariViewController.Configuration()
            // режим чтения
//            config.entersReaderIfAvailable = true

            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
   

}

//
//  NewsCollectionCell.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 23.03.2021.
//

import UIKit

class NewsCollectionCell: UICollectionViewCell {
    @IBOutlet weak var topicLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
    }
    
    func update(with text: String, selectedType: String) {
        topicLabel.text = text
        
        topicLabel.textColor = selectedType == text ? .white : Colors.mainBlue
        backgroundColor = selectedType == text ? #colorLiteral(red: 0.6435687542, green: 0.7348479629, blue: 0.8457549214, alpha: 1) : .clear
    }
    
}

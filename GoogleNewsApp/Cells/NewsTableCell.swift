//
//  NewsTableCell.swift
//  GoogleNewsApp
//
//  Created by Eduard Sinyakov on 23.03.2021.
//

import UIKit
import Kingfisher

class NewsTableCell: UITableViewCell {
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var sourceName: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var realContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        roundCorners(for: [realContentView, newsImageView])
    }

    func update(with model: Article) {
        if let urlToImage = model.urlToImage, let url = URL(string: urlToImage) {
            newsImageView.kf.setImage(with: url)
        }
        
        sourceName.text = model.source?.name
        newsTitle.text = model.title
    }
    
    private func roundCorners(for views:[UIView]) {
        views.forEach {
            $0.layer.cornerRadius = 10
            $0.layer.borderColor = UIColor.clear.cgColor
        }
    }

}
